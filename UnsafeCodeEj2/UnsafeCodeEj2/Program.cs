﻿using System;

namespace UnsafeCodeEj2
{
    class Program
    {
        static void Main(string[] args)
        {
            unsafe
            {
                int var = 20;
                int* p = &var;
                Console.WriteLine("Data is: {0} ", var);
                Console.WriteLine("Data is: {0} ", p->ToString());
                Console.WriteLine("Address is: {0} ", (int)p);
                Console.ReadKey();
            }
        }
    }
}
