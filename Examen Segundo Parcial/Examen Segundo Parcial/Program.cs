﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ExamenSegundoParcial
{
    class Program
    {
        private static Semaphore pooldechalanes;
        private static int _padding;
        private static int n = 0;
        private static int m = 0;
        private static int o = 0;
        private static int casasG;
        private static int[] habitaciones = new int[100];
        private static int[] baños = new int[100];
        public static void ChalanAlbanil(object chalan)
        {

            Console.WriteLine("Albañil");
            Console.WriteLine("Thread {0}: {1}, Priority {2}",
                        Thread.CurrentThread.ManagedThreadId,
                        Thread.CurrentThread.ThreadState,
                        Thread.CurrentThread.Priority);
            Console.WriteLine("Albañil {0} esperando turno...", chalan);
            pooldechalanes.WaitOne();
            // A padding interval to make the output more orderly.
            int padding = Interlocked.Add(ref _padding, 100);
            Console.WriteLine("Albañil {0} obtiene turno...", chalan);
            Thread.Sleep(1000 + padding);
            Console.WriteLine("Albañil {0} termina turno...", chalan);
            Console.WriteLine("ALbañil{0} libera su lugar {1}",
                chalan, pooldechalanes.Release());


        }

        public static void Main(string[] args)
        {
            //int casas;
            Console.WriteLine("¿Cuántos albañiles llegaron a trabajar?");
            n = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos albañiles estan dispobiles para trabajar?");
            m = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuantas casas desea contruir");
            casasG = Convert.ToInt32(Console.ReadLine());



            int i;
            for (i = 0; i < casasG; i++)
            {
                Console.WriteLine("Cuantas habitaciones tiene la casas {0}", i + 1);
                habitaciones[i] = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Cuantos baños tiene la casas {0}", i + 1);
                baños[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("¿Cuántos segundos hay para contruir?");
            o = Convert.ToInt32(Console.ReadLine()) * 1000;

            pooldechalanes = new Semaphore(0, m);
            /*for ( i = 1; i <= m; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(ChalanAlbanil));
                if (i % 2 == 0)
                {
                    t.Priority = ThreadPriority.AboveNormal;
                }
                else
                {
                    t.Priority = ThreadPriority.BelowNormal;
                }
                t.Start(i);
            }*/
            
            for (i = 1; i <= casasG; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(ConstruirCasas));
                t.Start(i);
            }

            Thread.Sleep(500);
            Console.WriteLine("Se libera un turno");
            pooldechalanes.Release(m);
            Thread.Sleep(o);
            Console.WriteLine("Se terminó la construcción");


        }

        public static void ConstruirCasas(object o)
        {
            pooldechalanes.WaitOne();
            var casas = new Task[casasG];
            for (int i = 0; i < casasG; i++)
            {
                Console.WriteLine("Construyendo la casa {0} ", i + 1);
                casas[i] = Task.Run(() => ChalanAlbanil(' '));
                
                for (int j=0; j<habitaciones[i]; j++)
                {
                    Console.WriteLine("Construyendo habitacion{0}", j + 1);
                }
                for (int j=0; j<baños[i]; j++)
                {
                    Console.WriteLine("Contruyendo baño{0}", j + 1);
                }
            }
            try
            {
                Task.WaitAll(casas);
                Console.WriteLine("------------------");
            }
            catch (AggregateException ae)
            {
                Console.WriteLine("One or more exceptions occurred: ");
                foreach (var ex in ae.Flatten().InnerExceptions)
                    Console.WriteLine("   {0}", ex.Message);
            }
            foreach (var p in casas)
                Console.WriteLine("Casa {0} Terminada", o);
        }


    }

}
