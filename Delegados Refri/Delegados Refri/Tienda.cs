﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Delegados_Refri
{
    class Tienda
    {
        public static void MandaViveres(int pKilos)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Mandaremos sus viveres, estoy en la tienda");
            Console.WriteLine("Seran {0} kilos", pKilos);
        }
    }
}
