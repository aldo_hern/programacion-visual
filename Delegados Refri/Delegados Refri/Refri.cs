﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Delegados_Refri
{
    //Delegados
    delegate void ReservasBajas(int kilos);
    delegate void Descongelado(int grados);
    class Refri
    {
        
        private int kilosAlimentos = 0;
        private int grados = 0;

        //Variables para invocar
        private ReservasBajas delReservas;
        private Descongelado delDescongelado;

        public Refri (int kilos, int pGrados)
        {
            //Valores iniciales del refri
            kilosAlimentos = kilos;
            grados = pGrados;
        }

        //Metodos que permiten refenciar las variables
        //Hacemos multicasting
        public void AdicionaMetodoReservas(ReservasBajas pMetodo)
        {
            delReservas += pMetodo;
        }
        public void EliminaMetodoReservas(ReservasBajas pMetodo)
        {
            delReservas -= pMetodo;
        }

        public void AdicionMetodoDescongelado(Descongelado pMetodo)
        {
            delDescongelado += pMetodo;
        }

        //Propiedades necesarias
        public int Kilos { get { return kilosAlimentos; } set { kilosAlimentos = value; } }
        public int Grados { get { return grados; } set { grados = value; } }


        public void Trabajar( int pConsumo)
        {
            //Actualizamos los kilos del refri
            kilosAlimentos -= pConsumo;

            //Subimos la temperatura
            grados += 1;

            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("{0} kilos, {1} grados", kilosAlimentos, grados);

            //Verificar si se cumple la condicion para
            //invocaar los handlers del evento

            //Condicion del evento para kilos

            if(kilosAlimentos<10)
            {
                //invocamos el metodo
                delReservas(kilosAlimentos);
            }

            //Esta es la condicion del evento para la temperatura
            if(grados>0)
            {
                //Invocamos el metodo
                delDescongelado(grados);
            }
        }
    }
}
