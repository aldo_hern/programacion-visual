﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace Delegados_Refri
{
    class Program
    {
        static void Main(string[] args)
        {
            Refri miRefri = new Refri(70, -20);
            Random r = new Random();
            //Variables para el multicasting
            ReservasBajas kilos1 = new ReservasBajas(InformeKilos);
            ReservasBajas kilos2 = new ReservasBajas(Tienda.MandaViveres);
            Descongelado desc1 = new Descongelado(InformeGrados);
            //Adicionamos los handlers
            miRefri.AdicionaMetodoReservas(kilos1);
            miRefri.AdicionaMetodoReservas(kilos2);
            miRefri.AdicionMetodoDescongelado(desc1);

            //Refri Trabaja

            while(miRefri.Kilos>0)
            {
                miRefri.Trabajar(r.Next(1, 5));
            }

            //Eliminamos un handler
            miRefri.EliminaMetodoReservas(kilos2);

            //Rellenamos el refri
            miRefri.Kilos = 40;
            miRefri.Grados = -10;
            while(miRefri.Kilos>0)
            {
                miRefri.Trabajar(r.Next(1, 5));
            }
        }

        public static void InformeKilos(int pKilos)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Reservas bajas de alimentos, estoy a nivel del main");
            Console.WriteLine("Quedan {0} kilos", pKilos);
        }

        public static void InformeGrados(int pGrados)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Se descongela el refri, estoy a nivel del main");
            Console.WriteLine("Esta a {0} grados", pGrados);
        }
    }
}
