﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Tutorial_de_LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            // Sintaxis de Consulta
            var startingDeck = (from s in Suits().LogQuery("Suit Generation")
                                from r in Ranks().LogQuery("Rank Generation")
                                select new { Suit = s, Rank = r }).LogQuery("Starting Deck");

            // Sintaxis de Metodo
            // var startingDeck = Suits().SelectMany(suit => Ranks().Select(rank => new { Suit = suit, Rank = rank }));
            // Display each card that we've generated and placed in startingDeck in the console

            //Dividimos a la mitad la baraja
            // 52 cartas en una baraja, entonces 52 / 2 = 26
            //var top = startingDeck.Take(26);
            // var bottom = startingDeck.Skip(26);
            // Ordenar la baraja aleatoriamente
            //var shuffle = top.InterleaveSequenceWith(bottom);
            /*foreach (var c in shuffle)
            {
                Console.WriteLine(c);
            }*/

            foreach (var c in startingDeck)
            {
                Console.WriteLine(c);
            }
            Console.WriteLine();
            var times = 0;
            var shuffle = startingDeck;
            do
            {
                //Orden no aleatorio
                //shuffle = shuffle.Take(26).InterleaveSequenceWith(shuffle.Skip(26));
                /*
                shuffle = shuffle.Take(26)
                    .LogQuery("Top Half")
                    .InterleaveSequenceWith(shuffle.Skip(26).LogQuery("Bottom Half"))
                    .LogQuery("Shuffle")
                    .ToArray();
                */
                //Orden Aleatorio
                //shuffle = shuffle.Skip(26).InterleaveSequenceWith(shuffle.Take(26));
                // In shuffle
                shuffle = shuffle.Skip(26)
                                       .LogQuery("Bottom Half")
                                       .InterleaveSequenceWith(shuffle.Take(26).LogQuery("Top Half"))
                                       .LogQuery("Shuffle")
                                       .ToArray();

                foreach (var c in shuffle)
                {
                    Console.WriteLine(c);
                }

                times++;
                Console.WriteLine(times);
            } while (!startingDeck.SequenceEquals(shuffle));

            Console.WriteLine(times);
        }
        static IEnumerable<string> Suits()
        {
            yield return "clubs";
            yield return "diamonds";
            yield return "hearts";
            yield return "spades";
        }

        static IEnumerable<string> Ranks()
        {
            yield return "two";
            yield return "three";
            yield return "four";
            yield return "five";
            yield return "six";
            yield return "seven";
            yield return "eight";
            yield return "nine";
            yield return "ten";
            yield return "jack";
            yield return "queen";
            yield return "king";
            yield return "ace";
        }
    }
}
