﻿using System;

namespace UnsafeCodeEj5
{
    class Program
    {
        unsafe static void Main(string[] args)
        {
            int[] list = { 10, 20, 30, 40, 50 };
            fixed (int* ptr = list)
                for (int i = 0; i < list.Length; i++)
                {
                    Console.WriteLine("Address of list[{0}]={1}", i, (int)(ptr + i));
                    Console.WriteLine("Value of list[{0}]={1}", i, *(ptr + i));
                }
            Console.ReadKey();
        }
    }
}
