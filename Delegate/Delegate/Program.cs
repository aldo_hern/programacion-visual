﻿using System;
    delegate int NumberChanger(int n);

namespace Delegate
{
        class TestDelegate
        {
            static int num = 10;

            public static int AddNum(int p)
            {
                num = p*p;
                return num;
            }
            public static int MultNum(int q)
            {
                num = q*q;
                return num;
            }
            public static int getNum()
            {
                return num;
            }
            static void Main(string[] args)
            {
                //create delegate instances
                NumberChanger nc1 = new NumberChanger(AddNum);
                NumberChanger nc2 = new NumberChanger(MultNum);

            //calling the methods using the delegate objects
                nc1(3);
                Console.WriteLine("Valor del numero 1 elevado al cuadrado: {0}", getNum());
                nc2(4);
                Console.WriteLine("Valor del numero 2 elevado al cuadrado: {0}", getNum());
                Console.ReadKey();
            }
        }
    }

