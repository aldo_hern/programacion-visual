﻿namespace Reloj_Checador
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxEmpleado = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.groupBoxOperacion = new System.Windows.Forms.GroupBox();
            this.rBtnEntrada = new System.Windows.Forms.RadioButton();
            this.rBtnSalida = new System.Windows.Forms.RadioButton();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.btnRevisar = new System.Windows.Forms.Button();
            this.lblEmpleado = new System.Windows.Forms.Label();
            this.lblFechaHr = new System.Windows.Forms.Label();
            this.groupBoxOperacion.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxEmpleado
            // 
            this.comboBoxEmpleado.FormattingEnabled = true;
            this.comboBoxEmpleado.Items.AddRange(new object[] {
            "Juan Hernandez",
            "Francisco Rivas",
            "Jose Perez",
            "Alberto Lopez"});
            this.comboBoxEmpleado.Location = new System.Drawing.Point(180, 92);
            this.comboBoxEmpleado.Name = "comboBoxEmpleado";
            this.comboBoxEmpleado.Size = new System.Drawing.Size(448, 21);
            this.comboBoxEmpleado.TabIndex = 0;
            this.comboBoxEmpleado.SelectedIndexChanged += new System.EventHandler(this.comboBoxEmpleado_SelectedIndexChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(180, 157);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(448, 20);
            this.dateTimePicker1.TabIndex = 1;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // groupBoxOperacion
            // 
            this.groupBoxOperacion.Controls.Add(this.rBtnSalida);
            this.groupBoxOperacion.Controls.Add(this.rBtnEntrada);
            this.groupBoxOperacion.Location = new System.Drawing.Point(180, 228);
            this.groupBoxOperacion.Name = "groupBoxOperacion";
            this.groupBoxOperacion.Size = new System.Drawing.Size(448, 61);
            this.groupBoxOperacion.TabIndex = 2;
            this.groupBoxOperacion.TabStop = false;
            this.groupBoxOperacion.Text = "Operacion";
            // 
            // rBtnEntrada
            // 
            this.rBtnEntrada.AutoSize = true;
            this.rBtnEntrada.Location = new System.Drawing.Point(85, 30);
            this.rBtnEntrada.Name = "rBtnEntrada";
            this.rBtnEntrada.Size = new System.Drawing.Size(62, 17);
            this.rBtnEntrada.TabIndex = 0;
            this.rBtnEntrada.TabStop = true;
            this.rBtnEntrada.Text = "Entrada";
            this.rBtnEntrada.UseVisualStyleBackColor = true;
            this.rBtnEntrada.CheckedChanged += new System.EventHandler(this.rBtnEntrada_CheckedChanged);
            // 
            // rBtnSalida
            // 
            this.rBtnSalida.AutoSize = true;
            this.rBtnSalida.Location = new System.Drawing.Point(276, 30);
            this.rBtnSalida.Name = "rBtnSalida";
            this.rBtnSalida.Size = new System.Drawing.Size(54, 17);
            this.rBtnSalida.TabIndex = 1;
            this.rBtnSalida.TabStop = true;
            this.rBtnSalida.Text = "Salida";
            this.rBtnSalida.UseVisualStyleBackColor = true;
            this.rBtnSalida.CheckedChanged += new System.EventHandler(this.rBtnSalida_CheckedChanged);
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Enabled = false;
            this.btnRegistrar.Location = new System.Drawing.Point(265, 317);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(85, 32);
            this.btnRegistrar.TabIndex = 3;
            this.btnRegistrar.Text = "Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // btnRevisar
            // 
            this.btnRevisar.Enabled = false;
            this.btnRevisar.Location = new System.Drawing.Point(456, 317);
            this.btnRevisar.Name = "btnRevisar";
            this.btnRevisar.Size = new System.Drawing.Size(85, 32);
            this.btnRevisar.TabIndex = 4;
            this.btnRevisar.Text = "Revisar";
            this.btnRevisar.UseVisualStyleBackColor = true;
            this.btnRevisar.Click += new System.EventHandler(this.btnRevisar_Click);
            // 
            // lblEmpleado
            // 
            this.lblEmpleado.AutoSize = true;
            this.lblEmpleado.Location = new System.Drawing.Point(101, 95);
            this.lblEmpleado.Name = "lblEmpleado";
            this.lblEmpleado.Size = new System.Drawing.Size(57, 13);
            this.lblEmpleado.TabIndex = 5;
            this.lblEmpleado.Text = "Empleado:";
            // 
            // lblFechaHr
            // 
            this.lblFechaHr.AutoSize = true;
            this.lblFechaHr.Location = new System.Drawing.Point(104, 163);
            this.lblFechaHr.Name = "lblFechaHr";
            this.lblFechaHr.Size = new System.Drawing.Size(68, 13);
            this.lblFechaHr.TabIndex = 6;
            this.lblFechaHr.Text = "Fecha/Hora:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblFechaHr);
            this.Controls.Add(this.lblEmpleado);
            this.Controls.Add(this.btnRevisar);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.groupBoxOperacion);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.comboBoxEmpleado);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBoxOperacion.ResumeLayout(false);
            this.groupBoxOperacion.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxEmpleado;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.GroupBox groupBoxOperacion;
        private System.Windows.Forms.RadioButton rBtnSalida;
        private System.Windows.Forms.RadioButton rBtnEntrada;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Button btnRevisar;
        private System.Windows.Forms.Label lblEmpleado;
        private System.Windows.Forms.Label lblFechaHr;
    }
}

