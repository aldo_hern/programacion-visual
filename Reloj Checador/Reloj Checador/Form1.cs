﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reloj_Checador
{
    public partial class Form1 : Form
    {
        DateTime entrada = new DateTime();
        DateTime salida = new DateTime();
        String nombre;
        int valEntSal = 0;

        string archivo = @"C:\Users\aldo_\source\repos\Reloj Checador\Registro.txt";
        public Form1()
        {
            InitializeComponent();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
        
            nombre = comboBoxEmpleado.Text;

            if (nombre != "")
            {
                if (!File.Exists(archivo))
                {
                    using (StreamWriter escribir = File.CreateText(archivo))
                    {
                        escribir.WriteLine("Nombre empleado:" + nombre);
                        if (valEntSal == 1)
                        {
                            escribir.WriteLine("Entrada:" + entrada + "\n");
                            MessageBox.Show("Entrada Registrada!");
                        }
                        else
                            if (valEntSal == 2)
                        {
                            escribir.WriteLine("Salida:" + salida+ "\n");
                            MessageBox.Show("Salida Registrada!");
                        }
                        
                       


                    }
                }
                else
                {
                    string nuevo = "";
                    if (valEntSal == 1)
                    {
                        nuevo = "Nombre empleado:" + nombre + "\nEntrada:" + entrada + "\n" + Environment.NewLine;
                        MessageBox.Show("Entrada Registrada!");
                    }
                    else
                        if (valEntSal == 2)
                    {
                        nuevo = "Nombre empleado:" + nombre + "\nSalida:" + salida + "\n" + Environment.NewLine;
                        MessageBox.Show("Salida Registrada!");
                    }
                    
                    File.AppendAllText(archivo, nuevo);


                }
            }
            else
            {
                MessageBox.Show("Seleccione un empleado!");
            }
            
        }

        private void btnRevisar_Click(object sender, EventArgs e)
        {
            if (File.Exists(archivo))
            {
                string leer = File.ReadAllText(archivo);
                MessageBox.Show(leer);
            }
            else
            {
                MessageBox.Show("El archivo aun no ha sido creado");
            }

        }

        private void rBtnEntrada_CheckedChanged(object sender, EventArgs e)
        {
            entrada = dateTimePicker1.Value;
            btnRegistrar.Enabled = true;
            btnRevisar.Enabled = true;
            valEntSal = 1;


        }

        private void rBtnSalida_CheckedChanged(object sender, EventArgs e)
        {
            salida = dateTimePicker1.Value;
            btnRegistrar.Enabled = true;
            btnRevisar.Enabled = true;
            valEntSal = 2; 
        }

        private void comboBoxEmpleado_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            entrada = dateTimePicker1.Value;
            salida = dateTimePicker1.Value;
        }
    }
}
