﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TaskB
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inicion Metodos");
            Task task1 = new Task(Impresiones);
            task1.Start();
            Task task2 = Task.Factory.StartNew(Pagos);
            
            Task task3 = Task.Run(() => { Nomina(); });
            task3.Wait();
            Console.WriteLine("Fin Metodos");
            Console.ReadKey();
        }

        static void Impresiones()
        {
            Console.WriteLine("Inicio impresiones");
            for (int count = 0; count <= 5; count++)
            {
                Console.WriteLine("Procesando impresiones {0}",count);
            }
            Console.WriteLine("Fin Impresiones");
        }
        static void Pagos()
        {
            Console.WriteLine("Inicio Pagos");
            for (int count = 0; count <= 5; count++)
            {
                Console.WriteLine("Procesando oagos {0}", count);
            }
            Console.WriteLine("Fin Pagos");
        }
        static void Nomina()
        {
            Console.WriteLine("Inicio Nomina");
            for (int count = 0; count <= 5; count++)
            {
                Console.WriteLine("Procesando Nomina {0}", count);
            }
            Console.WriteLine("Fin Nomina");
        }
    }
}