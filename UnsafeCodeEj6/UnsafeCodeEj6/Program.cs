﻿using System;

namespace UnsafeCodeEj6
{
    struct My_struct
    {
        public int Value1;
        public double Value2;
    }
    class Program
    {
        static unsafe void Main(string[] args)
        {
            My_struct refPoint = new My_struct();
            refPoint.Value1 = 20;
            refPoint.Value2 = 30;
            My_struct* pPoint = &refPoint;
            Console.WriteLine("Value1 = " + pPoint->Value2);
            Console.WriteLine("Value2 = " + pPoint->Value2);
            Console.WriteLine("value1 = " + (*pPoint).Value1);
            Console.WriteLine("value2 = " + (*pPoint).Value2);
            Console.ReadKey();
        }
    }
}
